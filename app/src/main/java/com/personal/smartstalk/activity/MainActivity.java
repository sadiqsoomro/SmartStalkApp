package com.personal.smartstalk.activity;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.personal.smartstalk.R;
import com.personal.smartstalk.fragments.OneFragment;
import com.personal.smartstalk.fragments.TwoFragment;
import com.personal.smartstalk.service.ClipBoardListenerService;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static String[] fragmentData = new String[2];


    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private static final String TAG = "sadiq";
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences sharedpreferences ;
    final String AutoLaunch = "AutoLaunch";
    SharedPreferences.Editor editor;

    private BroadcastReceiver activityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(TAG,"on receive fired");
            Bundle bundle = intent.getBundleExtra("msg");
            if(bundle != null)
            sendNotification(bundle.getString("msgBody"),bundle.getString("msgTitle"));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentData[0] = "false";
        fragmentData[1] = "";
        sharedpreferences = getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        ServiceStateDecider();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        if (activityReceiver != null) {
            IntentFilter intentFilter = new IntentFilter("ACTION_STRING_ACTIVITY");
            registerReceiver(activityReceiver, intentFilter);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void sendNotification(String body,String title) {

        //Get an instance of NotificationManager//
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.icon)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setSound(alarmSound);

        NotificationManager mNotificationManager =

                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0,
                notificationIntent, 0);

        mBuilder.setContentIntent(intent);
        mBuilder.setAutoCancel(true);

        mBuilder.setSound(Uri.parse("android.resource://"+getApplicationContext().getPackageName()+"/"+R.raw.notification));//Here is FILE_NAME is the name of file that you want to play

        if (mNotificationManager != null) {
            mNotificationManager.notify(001, mBuilder.build());
        }
    }
    public void selectFragment(int position){
        viewPager.setCurrentItem(position, true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ServiceStateDecider();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ServiceStateDecider();
    }

    @Override
    protected void onPause() {
        super.onPause();

        ServiceStateDecider();

    }

    @Override
    protected void onResume() {
        super.onResume();
        ServiceStateDecider();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setChecked(true);
        if(sharedpreferences.contains(AutoLaunch))
        {
             item = menu.findItem(R.id.action_settings);
            item.setChecked(sharedpreferences.getBoolean(AutoLaunch,false));
        }
        else
        {
             item = menu.findItem(R.id.action_settings);
            item.setChecked(true);
            editor.putBoolean(AutoLaunch, item.isChecked());
            editor.commit();
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.action_settings:
                item.setChecked(!item.isChecked());
                editor.putBoolean(AutoLaunch, item.isChecked());
                editor.commit();

                if(!item.isChecked() && isMyServiceRunning(ClipBoardListenerService.class))
                {
                    stopService(new Intent(this, ClipBoardListenerService.class));
                }

                if(item.isChecked() && !isMyServiceRunning(ClipBoardListenerService.class))
                {
                    startService(new Intent(this, ClipBoardListenerService.class));
                }

                return true;

        }
        return false;
    }

    private void setupViewPager(ViewPager viewPager) {
        MainActivity.ViewPagerAdapter adapter = new MainActivity.ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new OneFragment(), getString(R.string.Show_image_tab));
        adapter.addFragment(new TwoFragment(), getString(R.string.visited_list_tab));

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void ServiceStateDecider()
    {
        if(sharedpreferences.contains(AutoLaunch)) {
            if (!isMyServiceRunning(ClipBoardListenerService.class) && sharedpreferences.getBoolean(AutoLaunch,false))
                startService(new Intent(this, ClipBoardListenerService.class));
            else if (isMyServiceRunning(ClipBoardListenerService.class) && !sharedpreferences.getBoolean(AutoLaunch,false))
            {
                stopService(new Intent(this, ClipBoardListenerService.class));
            }
        }
    }
}
