package com.personal.smartstalk.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.personal.smartstalk.FireBase.AccessTokenSaver;
import com.personal.smartstalk.R;
import com.personal.smartstalk.classes.HelperClass;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import java.io.InterruptedIOException;
import java.net.URL;
import java.net.URLConnection;

public class login extends AppCompatActivity {

    EditText name, username;
    public Button saveDetails;
    boolean[] canSave = new boolean[2];
    HelperClass SharedPreferenceMethods;
    DownloadFileFromURL asyncTask = null;
    private ProgressDialog pDialog;
    public  String filename[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        SharedPreferenceMethods = new HelperClass(getApplicationContext());
        if (!SharedPreferenceMethods.FirstTimeLogin()) {
            Intent intent = new Intent(login.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        canSave[0] = false;
        canSave[1] = false;

        name = findViewById(R.id.input_name);
        username = findViewById(R.id.userInstaID);
        saveDetails = findViewById(R.id.btn_save);

        saveDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveDetailsEvent();
            }
        });
    }


    private void saveDetailsEvent() {
        if (!name.getText().toString().trim().isEmpty() && name.getText().toString().trim().length() > 3) {
            canSave[0] = true;
        } else {
            canSave[0] = false;
            name.setError("Enter your name");
        }

        if (!username.getText().toString().trim().isEmpty() && username.getText().toString().trim().length() > 3) {

            if(username.getText().toString().trim().split(" ").length ==1)
                canSave[1] = true;
            else
            {
                canSave[1] = false;
                username.setError("Instagram ID does not contain spaces");
            }

        } else {
            canSave[1] = false;
            username.setError("Enter your instagram ID");
        }

        if (canSave[0] && canSave[1]) {
            String instaId = username.getText().toString().trim().toLowerCase();
            if (SharedPreferenceMethods.isNetworkAvailable()) {

                if (asyncTask == null) {
                    asyncTask = new DownloadFileFromURL();
                }

                if (asyncTask.getStatus() != AsyncTask.Status.RUNNING) {


                    asyncTask = new DownloadFileFromURL();
                    asyncTask.execute("https://www.instagram.com/"+instaId);
                } else {
                    Toast.makeText(this, "Please Wait", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            saveDetails.setEnabled(false);
            pDialog = new ProgressDialog(login.this);
            pDialog.setMessage("Verifying ID, please wait");
            pDialog.setIndeterminate(true);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    asyncTask.cancel(true);
                    dialog.dismiss();
                }
            });
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... f_url) {
            try {
                String link="";
                try {
                    Document doc = null;
                    doc = Jsoup.connect(f_url[0]).timeout(6000).get();
                    if (isCancelled()) {
                        return "Cancelled By User";
                    }
                    Elements element = doc.head().getElementsByTag("meta");
                    link = element.get(14).attributes().getIgnoreCase("content");
                    filename = link.split("/");
                    if (isCancelled()) {
                        return "Cancelled By User";
                    }

                } catch (InterruptedIOException ex) {
                    return "connection";
                } catch (Exception ex) {
                    return "notfound";
                }

                URL url = new URL(link);
                if (isCancelled()) {
                    return "Cancelled By User";
                }
                URLConnection conection;
                try {
                    conection = url.openConnection();
                    conection.setConnectTimeout(6000);
                    if (isCancelled()) {
                        return "Cancelled By User";
                    }
                    conection.connect();
                } catch (Exception ex) {
                    return "connection";
                }

                if (isCancelled()) {
                    return "Cancelled By User";
                }
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                if (isCancelled()) {
                    return "Cancelled By User";
                }
                return "Length_"+String.valueOf(lenghtOfFile);
            } catch (Exception e) {
                return "Unable to connect to network";

            }


        }

        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setIndeterminate(false);
            pDialog.setMax(100);

            pDialog.setProgress(Integer.parseInt(progress[0]));
        }
        @Override
        protected void onCancelled(String xyz) {

            // Toast.makeText(MainActivity.this, xyz, Toast.LENGTH_LONG).show();
            pDialog.dismiss();
            saveDetails.setEnabled(true);

        }




        @Override
        protected void onPostExecute(String file_url) {
            switch (file_url) {
                case "notfound":

                    pDialog.dismiss();
                    saveDetails.setEnabled(true);
                    Toast.makeText(getApplicationContext(), "Unable to authenticate, ID not found.", Toast.LENGTH_SHORT).show();
                    break;
                case "Unable":

                    pDialog.dismiss();
                    saveDetails.setEnabled(true);
                    Toast.makeText(getApplicationContext(), "User not found", Toast.LENGTH_SHORT).show();
                    break;

                case "connection":
                    pDialog.dismiss();
                    saveDetails.setEnabled(true);
                    Toast.makeText(getApplicationContext(), "Connection Timeout, Please Try Later", Toast.LENGTH_SHORT).show();
                    break;
                case "Unable to connect to network":
                    pDialog.dismiss();
                    saveDetails.setEnabled(true);
                    Toast.makeText(getApplicationContext(), "Write Permission Error, grant the permission.", Toast.LENGTH_SHORT).show();
                    break;
                default:

                    pDialog.cancel();
                    saveDetails.setEnabled(true);
                    SharedPreferenceMethods.SetDataInSharedPreferences(username.getText().toString().trim().toLowerCase(), name.getText().toString().trim().toLowerCase());
                    AccessTokenSaver.SaveAccessTokenInFireBaseOnLogin(getApplicationContext());
                    Intent intent = new Intent(login.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                    break;
            }
        }
    }


}

