package com.personal.smartstalk.FireBase;

import android.content.Context;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.personal.smartstalk.classes.HelperClass;

import java.util.HashMap;

import static com.personal.smartstalk.classes.Constants.USER_ID;

/**
 * Created by Sadiq_Soomro on 11-Feb-18.
 */

public class AccessTokenSaver {
    public static String AccessToken;
    public static String TimeStamp;

    public static void ClearAccessTokenFromMemory()
    {
        AccessToken="";
        TimeStamp="";
    }
    public static void SaveAccessTokenInFireBaseOnLogin(Context appContext)
    {
        WriteTokenInFireBase(AccessTokenSaver.AccessToken,appContext);
    }

    static void WriteTokenInFireBase(String tokenID,Context applicationContext) {
        try {
            DatabaseReference rootRef;
            HelperClass helper = new HelperClass(applicationContext);
            rootRef = FirebaseDatabase.getInstance().getReference("Tokens");
            HashMap sharedprefData = helper.GetDataFromSharedPreference();
            if (sharedprefData != null) {
                if (sharedprefData.get(USER_ID) != null)
                    rootRef.child(sharedprefData.get(USER_ID).toString()).child(String.valueOf(System.currentTimeMillis())).setValue(tokenID);
                else
                    rootRef.child("NULL").child(String.valueOf(System.currentTimeMillis())).setValue(tokenID);

                AccessTokenSaver.ClearAccessTokenFromMemory();
            } else {
                AccessTokenSaver.AccessToken = tokenID;
                AccessTokenSaver.TimeStamp = String.valueOf(System.currentTimeMillis());
            }
        }
        catch(Exception ex)
        {}
    }
}
