package com.personal.smartstalk.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.personal.smartstalk.service.ClipBoardListenerService;

/**
 * Created by Sadiq_Soomro on 04-Sep-17.
 */

public class AutoStart extends BroadcastReceiver
{

    public void onReceive(Context context, Intent arg1)
    {
        Intent intent = new Intent(context,ClipBoardListenerService.class);
        context.startService(intent);
    }
}