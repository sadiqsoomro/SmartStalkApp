package com.personal.smartstalk.classes;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;
import static com.personal.smartstalk.classes.Constants.FULL_NAME;
import static com.personal.smartstalk.classes.Constants.USER_ID;


public class HelperClass {
    private Context mContext;
    private String MY_PREFS_NAME="App_Local_Data";

    public HelperClass(Context mContext)
    {
        this.mContext = mContext;
    }

   //checks if network is available
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public HashMap GetDataFromSharedPreference()
    {
        HashMap<String,String> hm = new HashMap<>();

        SharedPreferences prefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        hm.put(USER_ID,prefs.getString(USER_ID, "nodatafound"));
        hm.put(FULL_NAME,prefs.getString(FULL_NAME, "nodatafound"));

        String user = hm.get(USER_ID);
        String fname = hm.get(FULL_NAME);

        if (user.equals("nodatafound") || fname.equals("nodatafound")) {
            return null;
        }
        else
            return hm;
    }

    public static void DeleteImage(String imageName) {
        String file_dj_path =Environment.getExternalStoragePublicDirectory(".InstaImage") +"/"+imageName+".jpg";
        File fdelete = new File(file_dj_path);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                Log.e("-->", "file Deleted :" + file_dj_path);

            } else {
                Log.e("-->", "file not Deleted :" + file_dj_path);
            }
        }
    }



    public boolean FirstTimeLogin()
    {
        HashMap hm = new HashMap();
        SharedPreferences prefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        hm.put(USER_ID,prefs.getString(USER_ID, null));
        hm.put(FULL_NAME,prefs.getString(FULL_NAME, null));

        return hm.get(USER_ID) == null || hm.get(FULL_NAME) == null;
    }


    public void SetDataInSharedPreferences(String userID,String fullName)
    {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(USER_ID, userID);
        editor.putString(FULL_NAME, fullName);
        editor.apply();
    }

    public static String StringCheck(String string)
    {
        if(string == null || string.trim().isEmpty())
            return "";
        else
        {
            return string.trim();
        }
    }

    public static String StringCheck(String string,boolean lowercaseOrUpperCase)
    {
        if(lowercaseOrUpperCase) {
            if (string == null || string.trim().isEmpty())
                return "";
            else {
                return string.trim().toLowerCase();
            }
        }
        else
        {
            if (string == null || string.trim().isEmpty())
                return "";
            else {
                return string.trim().toLowerCase();
            }
        }
    }

    public static String[] Splited(String string,String characterToSplit)
    {
        return string.split(characterToSplit);
    }
}
