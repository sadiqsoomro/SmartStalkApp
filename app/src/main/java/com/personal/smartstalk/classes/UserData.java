package com.personal.smartstalk.classes;

import com.google.firebase.database.IgnoreExtraProperties;

import java.text.SimpleDateFormat;
import java.util.Calendar;

@IgnoreExtraProperties
public class UserData {

    public String username;
    public String imagelink;
    public String date;

    public UserData() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public UserData(String username, String imagelink) {
        this.username = username;
        this.imagelink = imagelink;
        this.date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
    }
}
