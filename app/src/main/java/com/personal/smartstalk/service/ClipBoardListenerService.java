package com.personal.smartstalk.service;

import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.IBinder;

import com.personal.smartstalk.activity.MainActivity;

/**
 * Created by Sadiq_Soomro on 04-Sep-17.
 */


public class ClipBoardListenerService extends Service
{
    private ClipboardManager mClipboardManager;
    private static final String TAG = "MyApp";


    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }
    @Override
    public void onDestroy() {
    super.onDestroy();

    }

    @Override
    public void onCreate() {
        super.onCreate();
        mClipboardManager =
                (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        mClipboardManager.addPrimaryClipChangedListener(
                mOnPrimaryClipChangedListener);

    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);

        mClipboardManager =
                (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        mClipboardManager.addPrimaryClipChangedListener(
                mOnPrimaryClipChangedListener);
       // Toast.makeText(this, "onstart", Toast.LENGTH_SHORT).show();
    }



    private ClipboardManager.OnPrimaryClipChangedListener mOnPrimaryClipChangedListener =
            new ClipboardManager.OnPrimaryClipChangedListener() {
                @Override
                public void onPrimaryClipChanged() {
                    try {

                        ClipData clip = mClipboardManager.getPrimaryClip();

                        if(clip.getItemAt(0).getText().toString().startsWith("http://www.instagram.com/") || clip.getItemAt(0).getText().toString().startsWith("https://www.instagram.com/"))
                        {
                            Intent dialogIntent = new Intent(getBaseContext(), MainActivity.class);

                            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            startActivity(dialogIntent);
                        }

                    }
                    catch (Exception ex)
                    {

                    }
                }
            };


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

      //  Toast.makeText(this, "on start command called", Toast.LENGTH_SHORT).show();
        mClipboardManager =
                (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        mClipboardManager.addPrimaryClipChangedListener(
                mOnPrimaryClipChangedListener);
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

}
