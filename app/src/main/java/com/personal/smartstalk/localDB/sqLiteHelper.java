package com.personal.smartstalk.localDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Sadiq_Soomro on 09-Sep-17.
 */

public class sqLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "smartStalk.db";
    private static final String TABLE_NAME = "visited_ids";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_USER_NAME = "user_name";
    private static final String COLUMN_DATE_VISITED = "date_visited";
    private static final String COLUMN_FILE_NAME = "file_name";



    public sqLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(
                "create table "+TABLE_NAME+" " +
                        "("+COLUMN_ID+" integer primary key autoincrement, "+COLUMN_USER_NAME+"" +
                        " text,"+COLUMN_FILE_NAME+" text, "+COLUMN_DATE_VISITED+" date default CURRENT_DATE)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(sqLiteDatabase);
    }

    public boolean InsertRecord (String name, String phone, String email, String street,String place) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_USER_NAME, name);
        contentValues.put(COLUMN_FILE_NAME, phone);
        db.insert(TABLE_NAME, null, contentValues);
        return true;
    }

    public Cursor GetFileNamesOfUser(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select "+COLUMN_FILE_NAME+" from "+TABLE_NAME+" where "+COLUMN_USER_NAME+"='"+username+"'", null );
        return res;
    }

    public int CountRecords(String username)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCount= db.rawQuery("select count("+COLUMN_ID+") from "+TABLE_NAME+" where "+COLUMN_USER_NAME+"="+username+"", null);
        mCount.moveToFirst();
        int count= mCount.getInt(0);
        mCount.close();

        return count;
    }


}
