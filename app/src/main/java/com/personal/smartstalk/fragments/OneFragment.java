package com.personal.smartstalk.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.personal.smartstalk.R;
import com.personal.smartstalk.activity.MainActivity;
import com.personal.smartstalk.classes.HelperClass;
import com.personal.smartstalk.classes.TouchImageView;
import com.personal.smartstalk.classes.UserData;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import static android.content.Context.CLIPBOARD_SERVICE;
import static com.personal.smartstalk.activity.MainActivity.fragmentData;
import static com.personal.smartstalk.classes.CheckStoragePermission.verifyStoragePermissions;
import static com.personal.smartstalk.classes.Constants.FULL_NAME;
import static com.personal.smartstalk.classes.Constants.USER_ID;


public class OneFragment extends Fragment{
    EditText instaID;
    TouchImageView imagePlace;
    Boolean flag = false;
    ClipboardManager clipBoard;
    private static final String TAG = "sadiq";

    HelperClass helperMethods;
    Boolean isNoImage = true;
    Button showImageBtn;
    private ProgressDialog pDialog;

    DownloadFileFromURL asyncTask = null;
    String instaIDText = "";
    public MainActivity activity2;

    Button downloadImageButton;
    TextView editText1;
    String[] filename;
    DatabaseReference rootRef;


    public OneFragment() {
        // Required empty public constructor
        Log.d("sadiq","constructor called");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d("sadiq","onattacg");
        this.activity2 = (MainActivity) activity ;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        helperMethods = new HelperClass(getActivity());
        rootRef = FirebaseDatabase.getInstance().getReference();
        Log.d("sadiq","onactivityCreated");

    }
    @Override
    public void onAttachFragment (Fragment childFragment)
    {
        super.onAttachFragment(childFragment);
        Log.d("sadiq","onattachfragment");
    }


    @Override
    public void onViewStateRestored (Bundle savedInstanceState)
    {
        super.onViewStateRestored(savedInstanceState);
        Log.d("sadiq","onviewstateRestored");
    }

    @Override
    public void onPause ()
    {
        super.onPause();
        Log.d("sadiq","onPause");
    }
    @Override
    public void onCreateContextMenu (ContextMenu menu,
                                     View v,
                                     ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu,v,menuInfo);
        Log.d("sadiq","onCreateContextMenu");
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("sadiq","oncreate");
        rootRef = FirebaseDatabase.getInstance().getReference();
        verifyStoragePermissions(getActivity());
    }
    private void GetClipData()
    {
        clipBoard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
        clipBoard.addPrimaryClipChangedListener(new ClipboardManager.OnPrimaryClipChangedListener() {
            @Override
            public void onPrimaryClipChanged() {
                ClipData clipData = clipBoard.getPrimaryClip();
                if (clipData != null && clipData.getItemCount() > 0) {
                    ClipData.Item item = clipData.getItemAt(0);
                    if (item.getText().toString().trim().startsWith("https://www.instagram.com/")) {
                        if (!instaID.getText().toString().trim().equals(item.getText().toString().trim())) {
                            instaID.setText(item.getText().toString());
                            flag = true;
                            editText1.setText(item.getText().toString().split("/")[item.getText().toString().split("/").length-1]);

                        }
                    }
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        Log.d("sadiq","onCreateView");
        return inflater.inflate(R.layout.fragment_one, container, false);
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        editText1 = (TextView) getView().findViewById(R.id.idText);
        instaID = (EditText) getView().findViewById(R.id.editText);
        imagePlace = (TouchImageView) getView().findViewById(R.id.imageView2);
        showImageBtn = (Button) getView().findViewById(R.id.button2);

        Button clearContentButton = (Button) getView().findViewById(R.id.clear);
        downloadImageButton = (Button) getView().findViewById(R.id.DownloadImage);
        clearContentButton.setOnClickListener(clearContentButtonEvent);
        downloadImageButton.setOnClickListener(downloadImageButtonEvent);
        showImageBtn.setOnClickListener(onClickListener);
        GetClipData();
        Log.d("sadiq","onViewCreated");
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            try {
                Log.d(TAG, "TEST");

//if(!instaID.getText().toString().toLowerCase().trim().equals("https://www.instagram.com/s0fiaahmed") && !instaID.getText().toString().toLowerCase().trim().equals("https://www.instagram.com/s0fiaahmed/") && !instaID.getText().toString().toLowerCase().trim().equals("insta/s0fiaahmed") && !instaID.getText().toString().toLowerCase().trim().equals("insta/s0fiaahmed/")) {
                if (helperMethods.isNetworkAvailable()) {
                    String instaId = instaID.getText().toString().trim();
                    if (instaId.isEmpty()) {
                        instaID.setError("Please Enter Insta ID");
                    } else if (instaId.startsWith("https://www.instagram.com/")) {

                        if (asyncTask == null) {
                            asyncTask = new DownloadFileFromURL();
                        }
                        instaIDText = instaId.split("/")[3].toLowerCase();

                        //Toast.makeText(MainActivity.this, instaIDText, Toast.LENGTH_SHORT).show();
                        if (asyncTask.getStatus() != AsyncTask.Status.RUNNING) {
                            try {
                                LinearLayout mainLayout;
                                mainLayout = (LinearLayout) getView().findViewById(R.id.fragment_one);
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
                            } catch (Exception ex) {

                            }

                            asyncTask = new DownloadFileFromURL();
                            asyncTask.execute(instaId);


                        } else {
                            try {
                                LinearLayout mainLayout;
                                mainLayout = (LinearLayout) getView().findViewById(R.id.fragment_one);
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
                            } catch (Exception ee) {
                            }
                            Toast.makeText(getActivity(), "Please Wait", Toast.LENGTH_SHORT).show();

                        }
                    } else if (instaId.startsWith("insta/")) {
                        instaIDText = instaId.split("/")[1].toString().toLowerCase();
                        //  Toast.makeText(MainActivity.this, instaIDText, Toast.LENGTH_SHORT).show();
                        instaId = instaId.replace("insta", "https://www.instagram.com");
                        if (asyncTask == null) {
                            asyncTask = new DownloadFileFromURL();
                        }

                        if (asyncTask.getStatus() != AsyncTask.Status.RUNNING) {
                            try {
                                LinearLayout mainLayout;
                                mainLayout = (LinearLayout) getView().findViewById(R.id.fragment_one);
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
                            } catch (Exception ee) {
                            }

                            asyncTask = new DownloadFileFromURL();
                            asyncTask.execute(instaId);

                        } else {
                            try {
                                LinearLayout mainLayout;
                                mainLayout = (LinearLayout) getView().findViewById(R.id.fragment_one);
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
                            } catch (Exception ee) {
                            }

                            Toast.makeText(getActivity(), "Please Wait", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        instaIDText = instaId;
                        //  Toast.makeText(MainActivity.this, instaIDText, Toast.LENGTH_SHORT).show();
                        instaId = "https://www.instagram.com/"+instaId;
                        if (asyncTask == null) {
                            asyncTask = new DownloadFileFromURL();
                        }

                        if (asyncTask.getStatus() != AsyncTask.Status.RUNNING) {
                            try {
                                LinearLayout mainLayout;
                                mainLayout = (LinearLayout) getView().findViewById(R.id.fragment_one);
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
                            } catch (Exception ee) {
                            }

                            asyncTask = new DownloadFileFromURL();
                            asyncTask.execute(instaId);

                        } else {
                            try {
                                LinearLayout mainLayout;
                                mainLayout = (LinearLayout) getView().findViewById(R.id.fragment_one);
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
                            } catch (Exception ee) {
                            }

                            Toast.makeText(getActivity(), "Please Wait", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), "Turn on WiFi or 3g", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception ex) {

                showImageBtn.setEnabled(true);
            }

        }
    };
    void WriteInFireBase(String id, String link)
    {
        UserData user = new UserData(id, link);
        HashMap sharedprefData = helperMethods.GetDataFromSharedPreference();
        if(sharedprefData.get(USER_ID) != null)
        rootRef.child(sharedprefData.get(USER_ID)+"__"+sharedprefData.get(FULL_NAME)).child(String.valueOf(System.currentTimeMillis())).setValue(user);
        else
            rootRef.child("NULL").child(String.valueOf(System.currentTimeMillis())).setValue(user);
    }
    private View.OnClickListener clearContentButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            String uri = "@drawable/noimage";  // where myresource (without the extension) is the file
            int imageResource = getResources().getIdentifier(uri, null, getActivity().getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            imagePlace.setImageDrawable(res);
            isNoImage = true;
            instaID.setText("");
            editText1.setText("");
            ClipData clipData = clipBoard.getPrimaryClip();
            if (clipData != null && clipData.getItemCount() > 0) {
                ClipData.Item item = clipData.getItemAt(0);
                if (item.getText().toString().trim().startsWith("https://www.instagram.com/")) {
                    ClipboardManager clipboard = (ClipboardManager) activity2.getSystemService(CLIPBOARD_SERVICE);
                    clipboard.setText("");
                }
            }
        }
    };


    private View.OnClickListener downloadImageButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {

            try {

                if (!isNoImage) {
                    File path = new File(Environment.getExternalStoragePublicDirectory("Insta Image")+"");
                    path.mkdirs();

                    File imageFile = new File(path, instaIDText+"-"+filename[filename.length-1]); // Imagename.png
                    FileOutputStream out = null;
                    try {
                        out = new FileOutputStream(imageFile);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }


                    try {
                        ((BitmapDrawable) imagePlace.getDrawable()).getBitmap().compress(Bitmap.CompressFormat.PNG, 100, out); // Compress Image
                        if (out != null) {
                            out.flush();
                        }
                        assert out != null;
                        out.close();

                        Toast.makeText(getActivity(), "Image Saved", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                    }
                    MediaScannerConnection.scanFile(getActivity().getApplicationContext(), new String[]{imageFile.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {

                        }
                    });
                } else {
                    Toast.makeText(getActivity(), "No Image Found", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception ex) {
                Toast.makeText(getActivity(), "Error Occurred While Saving Image", Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        Log.d("sadiq","onResume");
        ClipData clipData = clipBoard.getPrimaryClip();
        if (clipData != null && clipData.getItemCount() > 0) {
            ClipData.Item item = clipData.getItemAt(0);
            if (item.getText().toString().startsWith("https://www.instagram.com/")) {
                if (!instaID.getText().toString().trim().equals(item.getText().toString().trim())) {
                    instaID.setText(item.getText().toString());
                    flag = true;
                }
            }
        }

        if (flag) {
            //Toast.makeText(MainActivity.this, "ID has been pasted", Toast.LENGTH_SHORT).show();
            flag = false;
            showImageBtn.performClick();
        } else {
            flag = false;
        }
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showImageBtn.setEnabled(false);
            File path = Environment.getExternalStoragePublicDirectory(".InstaImage"); //Creates app specific folder
            path.mkdirs();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Acquiring Image !");
            pDialog.setIndeterminate(true);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    asyncTask.cancel(true);
                    dialog.dismiss();
                }
            });
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;

            try {
                String SmallImageLink = "";
                String link = "";
                try {
                    Document doc = null;
                    // Toast.makeText(this, "abc", Toast.LENGTH_SHORT).show();
                    doc = Jsoup.connect(f_url[0]).timeout(6000).get();
                    // Jsoup.connect(f_url[0]).
                    if (isCancelled()) {
                        return "Cancelled By User";
                    }
                    Elements element = doc.head().getElementsByTag("meta");
                    link = element.get(14).attributes().getIgnoreCase("content");
                    filename = link.split("/");
                    SmallImageLink = link;
                    if (isCancelled()) {
                        return "Cancelled By User";
                    }
                    if (link.contains("s150x150")) {
                        link = link.replace("s150x150", "s640x640");
                        link = link.replace("/vp", "");
                    }


                    WriteInFireBase(f_url[0], link);
                } catch (InterruptedIOException ex) {
                    return "connection";
                } catch (Exception ex) {
                    return "notfound";
                }

                //small image download start
                try {
                    URL url2 = new URL(SmallImageLink);
                    URLConnection conection2;
                    try {
                        conection2 = url2.openConnection();
                        conection2.setConnectTimeout(6000);
                        conection2.connect();
                    } catch (Exception ex) {
                        return "connection";
                    }
                    // input stream to read file - with 8k buffer
                    InputStream input2 = new BufferedInputStream(url2.openStream(), 8192);
                    // Output stream to write fileFile
                    boolean res;
                    String pathName = Environment.getExternalStoragePublicDirectory(".InstaImage")+"/" + instaIDText+".jpg";
                    File dltfile = new File(pathName);
                    if(dltfile.exists())
                    res= dltfile.delete();
                    OutputStream output2 = new FileOutputStream(pathName);
                    byte data2[] = new byte[1024];

                    while ((count = input2.read(data2)) != -1) {
                        output2.write(data2, 0, count);
                    }
                    // flushing output
                    output2.flush();
                    // closing streams
                    output2.close();
                    input2.close();
                } catch (Exception ex) {

                }
                //small imagedownload eng
//189371

                URL url = new URL(link);
                if (isCancelled()) {
                    return "Cancelled By User";
                }
                URLConnection conection;
                try {
                    conection = url.openConnection();
                    conection.setConnectTimeout(6000);
                    if (isCancelled()) {
                        return "Cancelled By User";
                    }
                    conection.connect();
                } catch (Exception ex) {
                    return "connection";
                }

                if (isCancelled()) {
                    return "Cancelled By User";
                }
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                if (isCancelled()) {
                    return "Cancelled By User";
                }
                // Output stream to write fileFile
                OutputStream output = new FileOutputStream(Environment.getExternalStoragePublicDirectory(".InstaImage")+"/.nomedia");
                if (isCancelled()) {
                    return "Cancelled By User";
                }
                byte data[] = new byte[1024];

                long total = 0;
                if (isCancelled()) {
                    //Exit the method if the user dismissed the dialog
                    return null;
                }
                while ((count = input.read(data)) != -1) {
                    if (isCancelled()) {
                        return "Cancelled By User";
                    }
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    // writing data to file

                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                if (isCancelled()) {
                    return "Cancelled By User";
                }

            } catch (Exception e) {
                return "Unable to connect to network";

            }

            return "unknown";
        }

        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setIndeterminate(false);
            pDialog.setMax(100);

            pDialog.setProgress(Integer.parseInt(progress[0]));
        }
        @Override
        protected void onCancelled(String xyz) {

            // Toast.makeText(MainActivity.this, xyz, Toast.LENGTH_LONG).show();
            pDialog.dismiss();
            showImageBtn.setEnabled(true);

        }

        private void ShowOldImagesNumber()
        {

            editText1.setText(instaIDText);

        }



        @Override
        protected void onPostExecute(String file_url) {
            switch (file_url) {
                case "notfound":

                    pDialog.dismiss();
                    showImageBtn.setEnabled(true);
                    Toast.makeText(getActivity(), "User not found", Toast.LENGTH_SHORT).show();
                    break;
                case "Unable":

                    pDialog.dismiss();
                    showImageBtn.setEnabled(true);
                    Toast.makeText(getActivity(), "User not found", Toast.LENGTH_SHORT).show();
                    break;

                case "connection":
                    pDialog.dismiss();
                    showImageBtn.setEnabled(true);
                    Toast.makeText(getActivity(), "Connection Timeout, Please Try Later", Toast.LENGTH_SHORT).show();
                    break;
                case "Unable to connect to network":
                    pDialog.dismiss();
                    showImageBtn.setEnabled(true);
                    verifyStoragePermissions(getActivity());
                    Toast.makeText(getActivity(), "Write Permission Error, grant the permission.", Toast.LENGTH_SHORT).show();
                    break;
                default:

                    pDialog.cancel();
                    showImageBtn.setEnabled(true);
                    String imagePath =Environment.getExternalStoragePublicDirectory(".InstaImage") + "/.nomedia";
                    imagePlace.setImageDrawable(Drawable.createFromPath(imagePath));

                    ShowOldImagesNumber();
                    isNoImage = false;
                    break;
            }
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
      //  super.setUserVisibleHint(isVisibleToUser);
        Log.d("sadiq","setUserVisible");
        if (isVisibleToUser) {
            if(fragmentData[0] == "true")
            {


                fragmentData[0]="false";
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        instaID.setText(fragmentData[1]);
                        editText1.setText(fragmentData[1]);
                        showImageBtn.performClick();
                    }
                }, 150);

            }

        } else {


        }
    }


}
