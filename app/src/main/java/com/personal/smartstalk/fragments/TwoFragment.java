package com.personal.smartstalk.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.personal.smartstalk.R;
import com.personal.smartstalk.activity.MainActivity;
import com.personal.smartstalk.lazylist.LazyAdapter;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Comparator;

import static com.personal.smartstalk.activity.MainActivity.fragmentData;
import static com.personal.smartstalk.classes.HelperClass.DeleteImage;


public class TwoFragment extends Fragment{

    ListView list;
    LazyAdapter adapter;
    ViewPager viewPager;
    public TwoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_two, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       // CustomListAdapter adapter=new CustomListAdapter(getActivity(), itemname, imgid);
        list=(ListView)getView().findViewById(R.id.list);
        viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);

    }
    private void PopulateRecentlyVisitedUsers()
    {
        try {
            String[] listFile;
            File[] files = Environment.getExternalStoragePublicDirectory(".InstaImage").listFiles(new FilenameFilter() {

                @Override
                public boolean accept(File dir, String filename) {

                    return (filename.contains(".png") || filename.contains(".jpg"));
                }
            });
            if (files != null && files.length > 1) {
                Arrays.sort(files, new Comparator() {
                    @Override
                    public int compare(Object o1, Object o2) {

                        if (((File) o1).lastModified() > ((File) o2).lastModified()) {
                            return -1;
                        } else if (((File) o1).lastModified() < ((File) o2).lastModified()) {
                            return +1;
                        } else {
                            return 0;
                        }
                    }

                });
            }
            listFile = new String[files.length];
            for (int i = 0; i < files.length; i++)
                listFile[i] = files[i].getAbsolutePath();

            adapter = new LazyAdapter(getActivity(), listFile);
            list.setAdapter(adapter);
            SetSingleClickEventOnItems();
            SetLongClickEventOnItems();
        }
        catch(Exception ex)
        {
            Log.d("sadiq",ex.toString());
        }
    }

    private void SetSingleClickEventOnItems()
    {

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                final TextView tv_id = (TextView) view.findViewById(R.id.item);


                fragmentData[0] = "true";
                fragmentData[1] = tv_id.getText().toString();
                //error occured in oreo
                ((MainActivity)getActivity()).selectFragment(0); //1 is second position

            }
        });
    }

    private void SetLongClickEventOnItems()
    {

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view,
                                           final int pos, long id) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Delete From List")
                        .setMessage("Do you want to delete this user from visited list?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final TextView tv_id = (TextView) view.findViewById(R.id.item);
                                DeleteImage(tv_id.getText().toString().trim());
                                PopulateRecentlyVisitedUsers();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();
                return true;
            }
        });
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            PopulateRecentlyVisitedUsers();

        } else {


        }
    }

}
